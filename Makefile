default:
	javac -d ./ -cp src/ src/Car.java
	javac -d ./ -cp src/ src/CarSimulator.java
	javac -d ./ -cp src/ src/Highway.java
	jar cvf HW5.jar Car.class CarSimulator.class Highway.class
	rm Car.class
	rm CarSimulator.class
	rm Highway.class
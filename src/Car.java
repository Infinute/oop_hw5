/** Each class Car implement a car on the highway*/
public class Car{
    /** The current speed of the car*/ 
    private int speed;
    /** The current position of the car*/ 
    private int pos;
    /** The current decision of the car*/ 
    private int dec;
    /** As if the car is changing it's highway to another*/ 
    private boolean change;
    /** Constructor without any variables*/    
    public Car (){;}
    /** Constructor includes speed and position*/ 
    public Car (int speed, int pos){
	this.speed = speed;
	this.pos = pos;
    }
    /** Get the current speed of the car*/    
    public int get_speed(){
	return speed;
    }
    /** Get the current position of the car*/   
    public int get_pos(){
	return pos;
    }
    /** Refresh the position of the car*/   
    public void move(){
	this.pos+= this.speed;
    }
    /** Make decision by reference to front_car_pos and this.dec.
     *  <br>If this.dec == 0, make decision <br>
     *  When speeding up , set this.dec to the target speed and plus 20 <br>
     *  When slowing down,  set this.dec to the target speed and plus 10 <br>
     *  Each time step we minus this.dec with 10 <br>
     *  If 10 <= this.dec <= 14, we set this.speed and set this.dec to 0 <br>
     *  If smart mode is on (task4 & task5), speed would calculate again when altering
     * */   
    public void make_decision(int front_car_pos, boolean smart){
	if (this.dec >= 10 && this.dec <= 14)  {
	    this.dec -= 10;
	    this.speed = this.dec;
	    if (smart == true && (front_car_pos-this.pos)/2 < this.speed)
		this.speed -= 1;
	    this.dec = 0;
	}
	if (this.dec == 0){
	    this.dec = (front_car_pos-this.pos)/2;
	    if (this.dec > 4)
		this.dec = 4;
	    if (this.dec > this.speed)
		this.dec += 20;
	    else if (this.dec < this.speed)
		this.dec += 10;
	    else
		this.dec = 0;
	} else if (this.dec >= 20){
	    this.dec-=10;
	}
    }
    /** set current speed, used when initial speed is larger than 4*/
    public void set_speed(int speed){
	this.speed = speed;
    }
    /** Get the current decision of the car, which only used for debugging*/   
    public int get_dec(){
	return dec;
    }   
    /** Get if the car is changing it's highway to another*/ 
    public boolean get_change(){
	if ( change == true){
	    change = false;
	    return true;
	} else
	    return false;
    }
    /** Set if the car changes it's highway to another*/ 
    public void set_change(){
	change = true;
    }
}
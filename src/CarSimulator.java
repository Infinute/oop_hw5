import java.awt.*;

import java.applet.*;

import java.awt.event.*;
/*
public class CarSimulator extends Applet{
    public void init(){
	TextField text1= new TextField ("TextField",20);	
//	text1.addActionListener(this);
	TextArea text2 = new TextArea ("TextArea",4,20);
//	text2.addTextListener(this);
	add(text1);
	add(text2);
    }
	public void textValueChanged(TextEvent e)
	{
	} 
}
*/

public class CarSimulator extends Applet implements ActionListener{
    TextArea text_show;
    TextField text_length;
    TextField text_car;
    Checkbox show_num;
    static int highway_len;
    static int num_car_at_beginning;
    int last = 2000;
//    int last_after = 2000;
    public void init(){
	Label label1 = new Label("The Length of highway:");
	text_length= new TextField ("",5);	
	Label label2 = new Label("The number of car:");
	text_car= new TextField ("",5);	
//	text1.addActionListener(this);
	text_show = new TextArea ("The Result will be shown here.",50,100);
//	text2.addTextListener(this);
	show_num = new Checkbox ("show the speed");
	Button start = new Button("Start");
	start.addActionListener(this);
	add(label1);
	add(text_length);
	add(label2);
	add(text_car);
	add(show_num);
	add(start);
	add(text_show);
    }
	public void actionPerformed(ActionEvent e)
	{
	    text_show.setText("");
	    
	    highway_len = Integer.valueOf(text_length.getText());
	    num_car_at_beginning = Integer.valueOf(text_car.getText());
	    
//	    int num_car_at_intersection = 0;
	    int times = 200;
	    boolean show_speed = false;
	    if (show_num.getState())
		show_speed = true;
		/*
		for (int i = 0 ; i < args.length; i++){
		    if (args[i].equals("-s")){
			show_speed = true;
			break;
		    }
		}*/
		/*
		if (args.length >= 4 && (args[0].equals("1") || args[0].equals("2") )){
		    if (show_speed == true & args.length==5)
			times = Integer.valueOf(args[3]);	
		    else if  (show_speed == false)
			times = Integer.valueOf(args[3]);
		} else if (args.length >= 5)
		    if (show_speed == true & args.length==6)
			times = Integer.valueOf(args[4]);
		    else if (show_speed == false)
		    	times = Integer.valueOf(args[4]);
			*/
		Highway[] highway = new Highway[highway_len];
//		Highway[] highway2 = new Highway[highway_len];
		/* print the initial status of the highway*/
		for (int j = 0; j < highway_len; j++){
        		text_show.append(".");
	    	}
    		text_show.append("\n");
		for (int i = 0; i < highway_len; i++){
		    highway[i]= new Highway();
		}

		/* set task*/
		//boolean case2 = false;
		//boolean case4 = false;
		//boolean case5 = false;
		/*
		if ( args[0].equals("2") )
		    case2 = true;
		if ( args[0].equals("3") ){
		    num_car_at_intersection = Integer.valueOf(args[3]);
		}
		if ( args[0].equals("4") ){
		    case4 = true;
		    num_car_at_intersection = Integer.valueOf(args[3]);
		}	    
		if ( args[0].equals("5") ){
		    case5 = true;
		    num_car_at_intersection = Integer.valueOf(args[3]);
		    for (int i = 0; i < highway_len; i++){
			 highway2[i]= new Highway();
		    }
		}*/
		/* Start the loop*/
		//if (case5 == false){
		for (int j = highway_len-1; j >= 0; j--)
		    highway[j].start();		
		    for (int i = 0; i < times; i++){
			/* Add a car at the beginning mark if available*/
			if (num_car_at_beginning > 0 && last > 1){		    
			    num_car_at_beginning--;
			    highway[0].tile = new Car(last/2,0);
			    if (highway[0].tile.get_speed() > 4)
				highway[0].tile.set_speed(4);
			    highway[0].last = last;
			}
			/* Add a car at the 50th mark if can*/
/*			if (num_car_at_intersection > 0 && highway[49].tile == null && highway[50].tile == null ){
			    boolean safe_for_intersection = true;
			    if (case4 == true){
	        		    for (int j = 43; j <= 48; j++){
	        			if  (highway[j].tile != null)
	        			    safe_for_intersection = false;
	        		    }
			    }
			    if (safe_for_intersection == true){
	        		    num_car_at_intersection--;
	        		    int speed = 1;
	        		    for (int j = 51; j < 58; j++){
	        			speed++;
	        			if  (highway[j].tile != null)
	        			    break;
	        		    }
	        		    highway[49].tile = new Car(speed/2,49);
	        		    if (highway[49].tile.get_speed() > 4)
	        			highway[49].tile.set_speed(4);
			    }
			}*/
			/* Making decision ,handling decision then move cars*/
/*			synchronized(this){
				notifyAll();
			}*/
			for (int j = highway_len-1; j >= 0; j--)
			    highway[j].done = false;
			for (int j = highway_len-1; j >= 0; j--)
				while (highway[j].done == false)
				    continue;
		    	last = 2000;
			for (int j = highway_len-1; j >= 0; j--){
			    if (highway[j].tile != null)
	                	if (highway[j].tile.get_pos() < highway_len){
	                	    highway[highway[j].tile.get_pos()].tile = highway[j].tile;
	                	    highway[highway[j].tile.get_pos()].last = last;
	                	    last = highway[j].tile.get_pos();        	    
	                    	    if (highway[j].tile.get_speed() > 0 )
	                    		highway[j].tile = null;
	                	} else 
	                	    highway[j].tile = null;
	        	}
			String a = "";			
			if (show_speed == false){
	                    	for (int j = 0; j < highway_len; j++){                   	    
	                    	    if (highway[j].tile == null)
	                    		a += ".";
	                    		//text_show.append(".");
	                    	//	System.out.print(".");
	                    	    else {
	                    		a += "x";
	                    		//text_show.append("x");
	                    	//	 System.out.print("x");
	                    	    }
	                    	}
			} else {
	                	for (int j = 0; j < highway_len; j++){
	                    	    if (highway[j].tile == null)
	                    		a += ".";
	                    		//text_show.append(".");
	                    		//System.out.print(".");
	                    	    else {
	                    		Integer speed = highway[j].tile.get_speed();
	                    		a += speed.toString();
	                    		//text_show.append(speed.toString());
	                    		 //System.out.print(highway[j].tile.get_speed());
	                    	    }
	                    	}
			}
			a += "\n" ;
	                text_show.append(a);
	        	//System.out.println("");
		    }   
		}
	//} 	
}

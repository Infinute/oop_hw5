/** Implement the highway as highway-length x class Highway.
 *  <br> Each class Highway means a mark of the highway
 * */
import java.lang.Thread;

public class Highway extends Thread{
    Car tile;
    int last = 2000;
    boolean done = true;
    public Highway(){
	this.tile = null;
    }
    public void run(){
	/*
	while(true){
	    try{
		wait();
	    } catch (InterruptedException e){
        	tile.make_decision(last, false);
        	tile.move();
        	done = true;
	    }
	}*/

	while(true){
	    if (done==false){
                if (tile != null){
        		tile.make_decision(last, false);
        		/** move cars*/
        		tile.move();
                }
		done = true;
	    }
	}
    }
}